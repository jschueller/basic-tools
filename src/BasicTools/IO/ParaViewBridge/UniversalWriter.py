# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE.txt', which is part of this source code package.
#

from BasicTools.IO.UniversalWriter import PopulateMeshFromVtkAndWriteMesh
PopulateMeshFromVtkAndWriteMesh(filename,self.GetInput())
